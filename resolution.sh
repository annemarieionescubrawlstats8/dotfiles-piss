#!/bin/fish
 #nm-applet # Internet selector
# /usr/lib/xfce-polkit/xfce-polkit &
 xset r rate 360 60 # making keys feel smoother
xrandr --dpi 90
 xrandr --output DVI-0 --set TearFree on # Turning "VSYNC" on
 picom --experimental-backends -b # Compositor
 xrandr --output DVI-0 --mode 1280x840_75.00
 xrandr --output DVI-0 --set "scaling mode" "Center"
 #nitrogen --restore
 feh --randomize --bg-fill ~/Pictures/wallpapers/arch.jpg
 #$HOME/.config/polybargit/launch.sh # turned off cuz qtile flex
 #wal -R
 wait 1
   

#  feh --randomize --bg-fill ~/Poze/arch.webp
